#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    This script retrieves information from guestInfo.ovfEnv and
    print the OVF properties.

Write config
    ##Depends on
    #epel-release
    #python-networkmanager
    #python-six
Read Vars
    Depends:
        open-vm-tools or Vmware tools
All rpms
yum install open-vm-tools epel-release -y
yum install python-networkmanager python-six -y
"""
import subprocess
from xml.dom.minidom import parseString
from pprint import pprint
import sys
import NetworkManager
import uuid
import socket
import json

modedhcp = False
ovfmd='/root/setovf.md'
ovfenv_cmd="/usr/bin/vmtoolsd --cmd 'info-get guestinfo.ovfEnv'"

##Read ovf env vars
def get_ovf_properties():
    """
        Return a dict of OVF properties in the ovfenv
    """
    properties = {}
    xml_parts = subprocess.Popen(ovfenv_cmd, shell=True,
                                 stdout=subprocess.PIPE).stdout.read()
    raw_data = parseString(xml_parts)
    for property in raw_data.getElementsByTagName('Property'):
        key, value = [ property.attributes['oe:key'].value,
                       property.attributes['oe:value'].value ]
        properties[key] = value
    return properties

properties = get_ovf_properties()

##Read interfaces
c = NetworkManager.const
device  = ""

for dev in NetworkManager.NetworkManager.GetDevices():
    if  dev.Managed:
        print("%-10s %-19s %-20s %s" % (dev.Interface, c('device_state', dev.State), dev.Driver, dev.Managed))
        device = dev
        devicename =  dev.Interface
        print "Using device " + devicename  + " to setup ovfEnv"

# print "device:"
# print device
# print properties['hostname']
# print properties['ipaddress']
# print properties['prefix']
# print properties['gateway']
# print properties['nameserver']
# print properties['domain']

try:
    properties['ipaddress']
except:
    modedhcp = True
    print  "ipaddress does not exists"

try:
    properties['gateway']
except:
    modedhcp = True
    print  "gateway does not exists"

try:
    properties['prefix']
except:
    modedhcp = True
    print  "prefix does not exists"


##Create a network named firstethernet and apply all Vars from ovfEnv
def writeconfigmanual():
    print "Creating and activating firstethernet Connection with fixed IP"
    firstethernet = {
     'connection': {'id': 'firstethernet',
                    'type': '802-3-ethernet',
                    'uuid': str(uuid.uuid4()),
                    'autoconnect': 1,
                    'interface-name': devicename
                    },
     'ipv4': {'method': 'manual',
     'dns': [properties['nameserver']],
     'addresses': [ [properties['ipaddress'], properties['prefix'], properties['gateway']] ],
     'dns-search': [ properties['domain'] ]
     },
     'ipv6': {'method': 'ignore'}
    }

    NetworkManager.Settings.AddConnection(firstethernet)

##Create a network named firstethernet and apply all Vars from ovfEnv
def writeconfigdhcp():
    print "Creating and activating firstethernet Connection with DHCP"
    firstethernet = {
     'connection': {'id': 'firstethernet',
                    'type': '802-3-ethernet',
                    'uuid': str(uuid.uuid4()),
                    'autoconnect': 1,
                    'interface-name': devicename
                    },
     'ipv4': {'method': 'auto'
     },
     'ipv6': {'method': 'ignore'}
    }

    NetworkManager.Settings.AddConnection(firstethernet)

#Change Hostname
def ChangeHostname():
    print "Changing hostname..."
    NetworkManager.Settings.SaveHostname(properties['hostname']+'.'+properties['domain'])
    print "Hostname changed."

#Touch file (with ovfEnv content) so the service wont start again
def createfile():
    print "Writing file so the service wont start again"
    f= open(ovfmd,"w+")
    f.write(json.dumps(properties))
    f.close

ChangeHostname()
if modedhcp == False:
    writeconfigmanual()

if modedhcp == True:
    writeconfigdhcp()

createfile()
