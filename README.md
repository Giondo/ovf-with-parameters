# OVF with  parameters

## Need to deploy a  OVF image with  parameters and set them inside the VM


### Install dependencies

```bash
yum install open-vm-tools epel-release -y
yum install python-networkmanager python-six -y
```


### How to use it

```bash
cp setovfenv.py /usr/local/sbin/
cp setovf.service /etc/systemd/system/
systemctl enable setovf.service
```

For the script to work ok you must set all the variables listed below:

```
inject_ovf_env: true
properties:
  gateway: "192.168.10.1"
  domain: "virtualinfra.online"
  ipaddress: "192.168.10.125"
  prefix: "24"
  nameserver: "8.8.8.8"
  hostname: "helper01"
```
if ipaddress or gateway or prefix is not set script will set the first ethernet as DHCP

---

None of references does what I needed, ovfconf was almost perfect but lack of netmask and is not parsing the XML properly
rc.local script is good but also not working ok not parsing the xml correctly so I search for a python parser and put everything together

Why python instead of bash?
Better control of what is happening less crapy code

References:

https://github.com/StefanHeimberg/ovfconf

https://github.com/lamw/custom-virtual-appliances/blob/master/rc.local

https://gist.github.com/lrivallain/77d9dda42bf77ddce1fc3bf2ee69e37a

https://medium.com/@f.i.rabby/python-network-manager-tutorial-c504cdb8fc76

https://github.com/seveas/python-networkmanager/
